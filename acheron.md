## LetsEncrypt cert for Webmin

Grab IAM keys for the acheron-dns user, then issue a cert:
`acme.sh --issue --dns dns_aws -d $domain`

**NB:** acme.sh will persist the keys. Make sure the IAM policy associated with them is as limited as possible. I used:

```
{
    "Version": "2012-10-17",
    "Id": "letsencrypt-route53-my-internal-domain",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "route53:ListHostedZones"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:GetHostedZone",
                "route53:ChangeResourceRecordSets",
                "route53:ListResourceRecordSets"
            ],
            "Resource": [
                "arn:aws:route53:::hostedzone/my-hosted-zone"
            ]
        }
    ]
}
```

Go to `~/.acme.sh/$domain`. Combine the cert and private key into one file, name it `miniserv.pem`. Don't be fooled by the .cer extension, the cert is actually in PEM format. `cat $domain.key $domain.cer > miniserv.pem && sudo cp miniserv.pem /etc/webmin && sudo systemctl restart webmin`

## BIND configuration

* SELinux will complain about logging config to anywhere other than /var/named/data. To fix this, run `setsebool -P named_write_master_zones 1`
* It's important to allow queries to both localhost and the server's public IP address, else network clients won't be able to query it:

```
options {
    listen-on port 53 { 127.0.0.1; <<server-public-IP>; };
    allow-query     { localhost; trusted; }; # "trusted" is, in this case, an RFC1918 range.
    <snip>
}
```

### Logging

* Logging is nice, so let's do ALL of it! Create a folder for the logs: `mkdir /var/named/logs && chown -R named:named /var/named/logs`
* Then add the following block to /etc/named.conf:

```

logging {
    channel default_file {
        file "/var/named/logs/default.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel general_file {
        file "/var/named/logs/general.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel database_file {
        file "/var/named/logs/database.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel security_file {
        file "/var/named/logs/security.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel config_file {
        file "/var/named/logs/config.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel resolver_file {
        file "/var/named/logs/resolver.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel xfer-in_file {
        file "/var/named/logs/xfer-in.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel xfer-out_file {
        file "/var/named/logs/xfer-out.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel notify_file {
        file "/var/named/logs/notify.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel client_file {
        file "/var/named/logs/client.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel unmatched_file {
        file "/var/named/logs/unmatched.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel queries_file {
        file "/var/named/logs/queries.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel network_file {
        file "/var/named/logs/network.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel update_file {
        file "/var/named/logs/update.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel dispatch_file {
        file "/var/named/logs/dispatch.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel dnssec_file {
        file "/var/named/logs/dnssec.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel lame-servers_file {
        file "/var/named/logs/lame-servers.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };

    category default { default_file; };
    category general { general_file; };
    category database { database_file; };
    category security { security_file; };
    category config { config_file; };
    category resolver { resolver_file; };
    category xfer-in { xfer-in_file; };
    category xfer-out { xfer-out_file; };
    category notify { notify_file; };
    category client { client_file; };
    category unmatched { unmatched_file; };
    category queries { queries_file; };
    category network { network_file; };
    category update { update_file; };
    category dispatch { dispatch_file; };
    category dnssec { dnssec_file; };
    category lame-servers { lame-servers_file; };
};
```

Then restart named.

### Block all the ads

Who needs a Pihole? Real heroes build their own using [RPZ](https://en.wikipedia.org/wiki/Response_policy_zone). Let's set one up to catch, and block, ads and trackers (with a bit of help).

First, we create an RPZ pointer in named.conf:

```
options {
    <snip>
    response-policy {
        zone "adblock.my.domain";
    };
```

Then, we create the zone for said RPZ:

```
zone "adblock.my.domain" {
        type master;
        file "/var/named/adblock.my.domain.hosts";
        allow-update { none; };
        allow-transfer { none; };
        allow-query { trusted; }; # This is important, else none of the queries from hosts on the LAN will make it to the RPZ!
};
```

Let's use Trellmor's awesome [zone file creator](https://github.com/Trellmor/bind-adblock) to populate the zone...

```
#!/bin/bash

yum install -y python36u

pip install requests dnspython
cd
git clone https://github.com/Trellmor/bind-adblock
cd bind-adblock

python3.6 update-zonefile.py /var/named/adblock.my.domain.hosts adblock.my.domain
```

The output should look something like this:

```
[root@acheron named]# head adblock.my.domain.hosts
@ 38400 IN SOA acheron.my.domain. dan.my.domain. 1538861511 10800 3600 604800 38400
@ 38400 IN NS acheron.my.domain.
0.r.msn.com IN CNAME .
0.start.bz IN CNAME .
000.0x1f4b0.com IN CNAME .
000.gaysexe.free.fr IN CNAME .
0000mps.webpreview.dsl.net IN CNAME .
0001.2waky.com IN CNAME .
000dom.revenuedirect.com IN CNAME .
000free.us IN CNAME .
```

See a problem here? Every single one of these domains will hit the root servers. Simple fix: Create an A record in another zone for a sinkhole IP, and replace the CNAME to the root servers with the contents of that A record.

Once this is done, restart named, and enjoy not being tracked on the Internet.


## Troubleshooting SELinux

SELinux is a pig. It's useful, but it's a pig. The errors it throws are cryptic, and it's often impossible to read them without some help. I found `sealert` to be quite useful. Install `setroubleshooting`. If you get any weird errors pertaining to file access, run audit.log through `sealert` to see whether SELinux broke something:

`sealert -a /var/log/audit/audit.log`

### Before sealert

```
[root@acheron var]# tail log/audit/audit.log
type=SERVICE_START msg=audit(1538925886.906:710): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=failed'
type=SERVICE_START msg=audit(1538925940.570:711): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named-setup-rndc comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=SERVICE_STOP msg=audit(1538925940.570:712): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named-setup-rndc comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=AVC msg=audit(1538925940.581:713): avc:  denied  { open } for  pid=21512 comm="named-checkconf" path="/var/named/adblock.my.domain.hosts" dev="dm-3" ino=33712696 scontext=system_u:system_r:named_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file
type=SYSCALL msg=audit(1538925940.581:713): arch=c000003e syscall=2 success=no exit=-13 a0=7f124cc4afe8 a1=0 a2=1b6 a3=24 items=0 ppid=21511 pid=21512 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm="named-checkconf" exe="/usr/sbin/named-checkconf" subj=system_u:system_r:named_t:s0 key=(null)
type=PROCTITLE msg=audit(1538925940.581:713): proctitle=2F7573722F7362696E2F6E616D65642D636865636B636F6E66002D7A002F6574632F6E616D65642E636F6E66
type=SERVICE_START msg=audit(1538925940.588:714): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=failed'
type=SERVICE_START msg=audit(1538925997.282:715): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named-setup-rndc comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=SERVICE_STOP msg=audit(1538925997.282:716): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named-setup-rndc comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
type=SERVICE_START msg=audit(1538925998.748:717): pid=1 uid=0 auid=4294967295 ses=4294967295 subj=system_u:system_r:init_t:s0 msg='unit=named comm="systemd" exe="/usr/lib/systemd/systemd" hostname=? addr=? terminal=? res=success'
```

### After sealert

```
[root@acheron var]# sealert -a /var/log/audit/audit.log
 79% donetype=AVC msg=audit(1538864435.173:225): avc:  denied  { write } for  pid=16390 comm="named" name="named" dev="dm-3" ino=33580170
scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:named_zone_t:s0 tclass=dir

**** Invalid AVC allowed in current policy ***

 80% donetype=AVC msg=audit(1538864608.765:232): avc:  denied  { write } for  pid=17711 comm="named" name="named" dev="dm-3" ino=33580170
scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:named_zone_t:s0 tclass=dir

**** Invalid AVC allowed in current policy ***

type=AVC msg=audit(1538864707.60:234): avc:  denied  { append } for  pid=17711 comm="named" name="network.log" dev="dm-3" ino=33710296 scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:named_zone_t:s0 tclass=file

**** Invalid AVC allowed in current policy ***

type=AVC msg=audit(1538864707.62:236): avc:  denied  { remove_name } for  pid=17711 comm="named" name="tmp-YWwntPGXON" dev="dm-3" ino=33710302 scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:named_zone_t:s0 tclass=dir

**** Invalid AVC allowed in current policy ***

type=AVC msg=audit(1538864707.62:235): avc:  denied  { create } for  pid=17711 comm="named" name="tmp-FJn4c1isx4" scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:named_zone_t:s0 tclass=file

**** Invalid AVC allowed in current policy ***

 91% donetype=AVC msg=audit(1538868192.817:477): avc:  denied  { read } for  pid=25209 comm="named" name="adblock.my.domain.hosts" dev="dm-3" ino=33712696 scontext=system_u:system_r:named_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file

**** Invalid AVC allowed in current policy ***

type=AVC msg=audit(1538868307.915:483): avc:  denied  { read } for  pid=25209 comm="named" name="adblock.my.domain.hosts" dev="dm-3" ino=33712696 scontext=system_u:system_r:named_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file

**** Invalid AVC allowed in current policy ***

100% done
found 1 alerts in /var/log/audit/audit.log
--------------------------------------------------------------------------------

SELinux is preventing /usr/sbin/named from open access on the file /var/named/adblock.my.domain.hosts.

*****  Plugin restorecon (99.5 confidence) suggests   ************************

If you want to fix the label.
/var/named/adblock.my.domain.hosts default label should be named_zone_t.
Then you can run restorecon. The access attempt may have been stopped due to insufficient permissions to access a parent directory in which case try to change the following command accordingly.
Do
# /sbin/restorecon -v /var/named/adblock.my.domain.hosts

*****  Plugin catchall (1.49 confidence) suggests   **************************

If you believe that named should be allowed open access on the adblock.my.domain.hosts file by default.
Then you should report this as a bug.
You can generate a local policy module to allow this access.
Do
allow this access for now by executing:
# ausearch -c 'named' --raw | audit2allow -M my-named
# semodule -i my-named.pp


Additional Information:
Source Context                system_u:system_r:named_t:s0
Target Context                unconfined_u:object_r:user_home_t:s0
Target Objects                /var/named/adblock.my.domain.hosts [ file ]
Source                        named
Source Path                   /usr/sbin/named
Port                          <Unknown>
Host                          <Unknown>
Source RPM Packages           bind-9.9.4-61.el7_5.1.x86_64
Target RPM Packages
Policy RPM                    selinux-policy-3.13.1-192.el7_5.6.noarch
Selinux Enabled               True
Policy Type                   targeted
Enforcing Mode                Enforcing
Host Name                     acheron.my.domain
Platform                      Linux acheron.my.domain
                              3.10.0-862.14.4.el7.x86_64 #1 SMP Wed Sep 26
                              15:12:11 UTC 2018 x86_64 x86_64
Alert Count                   4
First Seen                    2018-10-06 19:26:30 EDT
Last Seen                     2018-10-07 11:25:40 EDT
Local ID                      d95bbeb0-47ec-4261-b453-acd30d2b781f

Raw Audit Messages
type=AVC msg=audit(1538925940.581:713): avc:  denied  { open } for  pid=21512 comm="named-checkconf" path="/var/named/adblock.my.domain.hosts" dev="dm-3" ino=33712696 scontext=system_u:system_r:named_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file


type=SYSCALL msg=audit(1538925940.581:713): arch=x86_64 syscall=open success=no exit=EACCES a0=7f124cc4afe8 a1=0 a2=1b6 a3=24 items=0 ppid=21511 pid=21512 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm=named-checkconf exe=/usr/sbin/named-checkconf subj=system_u:system_r:named_t:s0 key=(null)

Hash: named,named_t,user_home_t,file,open
```