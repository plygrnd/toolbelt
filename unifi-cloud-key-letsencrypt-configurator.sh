#!/bin/bash

#################################################
#                                               # 
#                Unifi Cloud Key                #
#            LetsEncrypt Configurator           #
#                  Version 0.0.1                #
#                                               #
#################################################

# Most of this is automation of the work described in
# https://www.naschenweng.info/2017/01/06/securing-ubiquiti-unifi-cloud-key-encrypt-automatic-dns-01-challenge/

domain=$1
aws_akid=$2
aws_skid=$3

# Get acme.sh 

curl https://get.acme.sh | sh

# Make sure we pick up the changes...
reset

# Make sure we're staying current
acme.sh \
--upgrade \
--auto-upgrade \
--accountemail "my@email.domain"

# Post-cert-install hook shamelessly stolen from Nathan Schweng

cat << EOF > /root/.acme.sh/cloudkey-renew-hook.sh
#!/bin/bash
# Renew-hook for ACME / Let's encrypt
echo "** Configuring new Let's Encrypt certs"
cd /etc/ssl/private
rm -f /etc/ssl/private/cert.tar /etc/ssl/private/unifi.keystore.jks /etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/fullchain.pem

openssl pkcs12 -export -in /etc/ssl/private/cloudkey.crt -inkey /etc/ssl/private/cloudkey.key -out /etc/ssl/private/cloudkey.p12 -name unifi -password pass:aircontrolenterprise

keytool -importkeystore -deststorepass aircontrolenterprise -destkeypass aircontrolenterprise -destkeystore /usr/lib/unifi/data/keystore -srckeystore /etc/ssl/private/cloudkey.p12 -srcstoretype PKCS12 -srcstorepass aircontrolenterprise -alias unifi

rm -f /etc/ssl/private/cloudkey.p12
tar -cvf cert.tar *
chown root:ssl-cert /etc/ssl/private/*
chmod 640 /etc/ssl/private/*

echo "** Testing Nginx and restarting"
/usr/sbin/nginx -t
/etc/init.d/nginx restart ; /etc/init.d/unifi restart
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
	
#!/bin/bash
# Renew-hook for ACME / Let's encrypt
echo "** Configuring new Let's Encrypt certs"
cd /etc/ssl/private
rm -f /etc/ssl/private/cert.tar /etc/ssl/private/unifi.keystore.jks /etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/fullchain.pem
 
openssl pkcs12 -export -in /etc/ssl/private/cloudkey.crt -inkey /etc/ssl/private/cloudkey.key -out /etc/ssl/private/cloudkey.p12 -name unifi -password pass:aircontrolenterprise
 
keytool -importkeystore -deststorepass aircontrolenterprise -destkeypass aircontrolenterprise -destkeystore /usr/lib/unifi/data/keystore -srckeystore /etc/ssl/private/cloudkey.p12 -srcstoretype PKCS12 -srcstorepass aircontrolenterprise -alias unifi
 
rm -f /etc/ssl/private/cloudkey.p12
tar -cvf cert.tar *
chown root:ssl-cert /etc/ssl/private/*
chmod 640 /etc/ssl/private/*
 
echo "** Testing Nginx and restarting"
/usr/sbin/nginx -t
/etc/init.d/nginx restart ; /etc/init.d/unifi restart

EOF

# Sort the AWS creds out so we can talk to R53
export  AWS_ACCESS_KEY_ID=$aws_akid
export  AWS_SECRET_ACCESS_KEY=$aws_skid

# Actually issue the cert
acme.sh \
--force \
--issue \
--dns dns_aws \
-d $domain \
--pre-hook "touch /etc/ssl/private/cert.tar; tar -zcvf /root/.acme.sh/CloudKeySSL_`date +%Y-%m-%d_%H.%M.%S`.tgz /etc/ssl/private/*" \
--fullchainpath /etc/ssl/private/cloudkey.crt \
--keypath /etc/ssl/private/cloudkey.key \
--reloadcmd "sh /root/.acme.sh/cloudkey-renew-hook.sh"