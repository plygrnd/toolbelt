# Bash cheat sheet

## Processes

| **Command** |                    Result                    |
| :---------: | :------------------------------------------: |
|   Ctrl+C    |              SIGINT (Interrupt)              |
|   Ctrl+Z    | SIGTSTP (Suspend current foreground process) |
|   Ctrl+D    |              EOF (Close shell)               |

## Screen control

| Command |        Result        |
| :-----: | :------------------: |
| Ctrl+L  |     Clear screen     |
| Ctrl+S  |  Stop screen output  |
| Ctrl+Q  | Resume screen output |

## Moving the cursor

|   Command   |                           Result                           |
| :---------: | :--------------------------------------------------------: |
| Ctrl+A/Home |                     Beginning of line                      |
| Ctrl+E/End  |                        End of line                         |
|    Alt+B    |                       Back one word                        |
|   Ctrl+B    |                     Back one character                     |
|    Alt+F    |                      Forward one word                      |
|   Ctrl+F    |                   Forward one character                    |
|   Ctrl+XX   | Move between beginning of line and current cursor position |

## Deleting text

|   Command   |                       Result                       |
| :---------: | :------------------------------------------------: |
|   Ctrl+D    |           Delete character under cursor            |
|    Alt+D    | Delete all characters under cursor on current line |
| Ctrl+H/Bksp |           Delete character before cursor           |

## Typos

| Command |                            Result                            |
| :-----: | :----------------------------------------------------------: |
|  Alt+T  |             Swap current word with previous word             |
| Ctrl+T  | Swap the last two characters before the cursor with each other |
| Ctrl+_  |                      Undo last keypress                      |

## Cut/Paste

| Command |             Result             |
| :-----: | :----------------------------: |
| Ctrl+W  |     Cut word before cursor     |
| Ctrl+K  | Cut part of line after cursor  |
| Ctrl+U  | Cut part of line before cursor |
| Ctrl+Y  |         Paste last cut         |

## Capitalisation

| Command |                            Result                            |
| :-----: | :----------------------------------------------------------: |
|  Alt+U  | Capitalize every character from the cursor to the end of the current word, converting the characters to upper case |
|  Alt+L  | Uncapitalize every character from the cursor to the end of the current word, converting the characters to lower case |
|  Alt+C  |          Capitalize the character under the cursor           |

