#!/bin/bash

##########################################
# Docker CE installer for Ubuntu >18.04
# Author: Dan Urson (@plygrnd)
# Version: 0.1
##########################################

verify_docker_gpg_key () {
        docker_key=$(curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg - 2>/dev/null)
        echo -e "Docker's current GPG key:"
        echo $docker_key
        echo "Does this match the current key displayed at http://tiny.cc/qz2gzy ?"

        read response

        case $response in
                y|Y|yes|Yes) return 0;
                ;;
                n|N|no|No) return 1;
                ;;
        esac
}

check_if_running_as_root () {
        if [ "$EUID" -ne 0 ]
                then
                echo "Whoa there, check your privilege - this script needs root privileges"
                echo "*****DANGER DANGER DANGER DANGER DANGER*****"
                echo "DO NOT RUN AS ROOT WITHOUT FIRST INSPECTING THE CONTENTS OF THIS SCRIPT"
                echo "*****DANGER DANGER DANGER DANGER DANGER*****"
                exit 1
        fi
}

install_deps() {
        if check_if_deps_installed; then
                return 1
        fi
        apt install -y curl software-properties-common ca-certificates apt-transport-https
        return 0
}

install_docker_apt_key () {
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
}

add_stable_docker_repo () {
        add-apt-repository \
                "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
                $(lsb_release -cs) \
                stable"
}

install_docker () {
        check_if_running_as_root;
        if verify_docker_gpg_key; then
                install_docker_apt_key;
                add_stable_docker_repo;
                apt -y update;
                apt install -fy docker-ce;
        else
                echo "Whoops, we got a bullshit GPG key, dying gracefully"
                exit 1;
        fi
}

install_docker